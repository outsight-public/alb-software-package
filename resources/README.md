# Resources

We provide record files in this folder that you can use to run samples. 

> :warning: if this project was **cloned**, resource files won't be downloaded.

**Solutions :**
- Download this project as a zip

OR
- Set up git LFS

### Git LFS

If you want to use them to run samples on a **cloned** project, 
you will need git LFS set up on your system to download them.

LiDAR records are large and can't be directly stored in a git project. 
This is the reason why this project uses **git LFS** to manage large files.

#### Setup
- Install git LFS command line extension and setup git LFS 
by following **step 1** of this tutorial: https://git-lfs.github.com/

- Then pull files by running:
```shell
git lfs pull
```
