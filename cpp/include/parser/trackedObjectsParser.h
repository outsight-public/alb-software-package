#ifndef _TRACKED_OBJECTS_PARSER_H
#define _TRACKED_OBJECTS_PARSER_H

#include "osefParser.h"
#include "osefTypes.h"
#include "typeCaster.h"

// Class to read an osef stream and extract objects and zones.
class TrackedObjectsParser : public OsefParser {
    public:
	TrackedObjectsParser(const std::string &inputPath, const std::string &outputPath, bool autoReconnect = true);

    protected:
	// Parse the TLV frame and export data to the PLY.
	// Overrided function.
	// returns:
	// 0 in case of success.
	// -1 in error.
	int parseFrame(const Tlv::tlv_s *frame) override;

    private:
	// Write the header for the parser output.
	void writeCsvHeader();

	// Write object zones data to csv.
	void writeCsv(const TimestampMicroseconds::timestamp_s &timestamp, uint32_t number_of_objects,
		      const std::vector<uint32_t> &object_ids,
		      const std::vector<ZonesObjectsBinding32Bits::binding_s> &bindings,
		      const std::vector<std::string> &zone_names);

    private:
	std::string output_path;
};

#endif // _TRACKED_OBJECTS_PARSER_H