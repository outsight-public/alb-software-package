#include <fstream>
#include <iomanip>

#include "parser/trackedObjectsParser.h"

TrackedObjectsParser::TrackedObjectsParser(const std::string &inputPath, const std::string &outputPath,
					   bool autoReconnect)
	: OsefParser(inputPath, autoReconnect), output_path(outputPath)
{
	writeCsvHeader();
}

int TrackedObjectsParser::parseFrame(const Tlv::tlv_s *frame)
{
	if (!checkValidParsing(frame)) {
		return -1;
	}

	Tlv::Parser parser(frame->getValue(), frame->getLength());
	uint32_t number_of_objects;
	std::vector<uint32_t> object_ids;
	std::vector<ZonesObjectsBinding32Bits::binding_s> bindings;
	std::vector<std::string> zone_names;
	TimestampMicroseconds::timestamp_s timestamp;

	const Tlv::tlv_s *timeTlv = parser.findTlv(OSEF_TYPE_TIMESTAMP_MICROSECOND);
	const Tlv::tlv_s *frameTlv = parser.findTlv(OSEF_TYPE_SCAN_FRAME);

	if (!timeTlv || !frameTlv) {
		printf("[TrackedObjectsParser] Timestamp nand/or scan frame not found\n");
		return -1;
	}

	timestamp = TimestampMicroseconds::castValue(timeTlv);

	// Search for sub frame listing the tracked objects.
	Tlv::Parser frameParser(frameTlv->getValue(), frameTlv->getLength());
	const Tlv::tlv_s *objectsTlv = frameParser.findTlv(OSEF_TYPE_TRACKED_OBJECTS);
	const Tlv::tlv_s *zone_object_binding = frameParser.findTlv(OSEF_TYPE_ZONES_OBJECTS_BINDING);
	const Tlv::tlv_s *zone_object_binding_32b = frameParser.findTlv(OSEF_TYPE_ZONES_OBJECTS_BINDING_32_BITS);
	const Tlv::tlv_s *zonesDefTlv = frameParser.findTlv(OSEF_TYPE_ZONES_DEF);

	if (!objectsTlv || (!zone_object_binding && !zone_object_binding_32b) || !zonesDefTlv) {
		printf("[TrackedObjectsParser] Objects and/or Zones def and/or Zone-object bindings not found\n");
		return -1;
	}

	// Parse objects.
	Tlv::Parser objectsParser(objectsTlv->getValue(), objectsTlv->getLength());
	const Tlv::tlv_s *nbObjectsTlv = objectsParser.findTlv(OSEF_TYPE_NUMBER_OF_OBJECTS);
	const Tlv::tlv_s *object_ids_tlv = objectsParser.findTlv(OSEF_TYPE_OBJECT_ID);
	const Tlv::tlv_s *object_ids_32b_tlv = objectsParser.findTlv(OSEF_TYPE_OBJECT_ID_32_BITS);

	if (!nbObjectsTlv || (!object_ids_tlv && !object_ids_32b_tlv)) {
		printf("[TrackedObjectsParser] Number of objects and/or Object IDs not found\n");
		return -1;
	}

	number_of_objects = NumberOfObjects::castValue(nbObjectsTlv);
	if (object_ids_tlv) {
		std::vector<uint64_t> object_ids_64(ObjectIds::castValue(object_ids_tlv));
		object_ids = ObjectIds32Bits::castValue(&object_ids_64);
	} else {
		object_ids = ObjectIds32Bits::castValue(object_ids_32b_tlv);
	}

	// Parse Zones-Objects bindings.
	if (zone_object_binding) {
		std::vector<ZonesObjectsBinding::binding_s> bindings64 =
			ZonesObjectsBinding::castValue(zone_object_binding);
		bindings = ZonesObjectsBinding32Bits::castValue(&bindings64);
	} else {
		bindings = ZonesObjectsBinding32Bits::castValue(zone_object_binding_32b);
	}

	// Parse Zone names.
	Tlv::Parser zonesParser(zonesDefTlv->getValue(), zonesDefTlv->getLength());
	Tlv::tlv_s *zoneTlv;
	while ((zoneTlv = zonesParser.nextTlv())) {
		Tlv::Parser zoneParser(zoneTlv->getValue(), zoneTlv->getLength());
		const Tlv::tlv_s *ZoneNameTlv = zoneParser.findTlv(OSEF_TYPE_ZONE_NAME);
		if (ZoneNameTlv) {
			std::string zone_name{};
			zone_name.assign((char *)ZoneNameTlv->getValue());
			zone_names.push_back(zone_name);
		}
	}

	writeCsv(timestamp, number_of_objects, object_ids, bindings, zone_names);
	return 0;
}

void TrackedObjectsParser::writeCsvHeader()
{
	std::ofstream csv_file;
	csv_file.open(output_path);
	csv_file << "timestamp, object_id, zone names \n";
	csv_file.close();
}

void TrackedObjectsParser::writeCsv(const TimestampMicroseconds::timestamp_s &timestamp, uint32_t number_of_objects,
				    const std::vector<uint32_t> &object_ids,
				    const std::vector<ZonesObjectsBinding32Bits::binding_s> &bindings,
				    const std::vector<std::string> &zone_names)
{
	std::ofstream csv_file;
	csv_file.open(output_path, std::ios::app);
	double timestamp_f = (double)timestamp.seconds + (double)timestamp.micro_seconds * 0.000001f;
	for (int i = 0; i < number_of_objects; ++i) {
		csv_file << std::setprecision(6) << std::fixed << timestamp_f << ", ";
		csv_file << object_ids[i] << ", ";

		int bindingCount(0);
		for (int b = 0; b < bindings.size(); ++b) {
			if (object_ids[i] == bindings[b].obj_pid) {
				if (bindingCount > 1)
					csv_file << " | ";
				csv_file << zone_names[bindings[b].zone_index];
				++bindingCount;
			}
		}
		csv_file << "\n";
	}

	csv_file.close();
}