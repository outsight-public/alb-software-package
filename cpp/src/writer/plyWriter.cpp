// Local headers
#include "writer/plyWriter.h"

PlyWriter::PlyWriter(const std::string &outputPath) : nb_point_location(0), nb_points(0), output_path(outputPath)
{
	writePlyHeader();
}

void PlyWriter::writePoints(const Eigen::MatrixXf &points, const Eigen::VectorXi &reflectivities)
{
	// Open file for read and update, in binary.
	FILE *fp;
	fp = fopen(output_path.c_str(), "rb+");
	if (!fp) {
		printf("[PlyWriter] Unable to open the file.");
		return;
	}
	fseek(fp, 0, SEEK_END);

	for (size_t colIndex = 0; colIndex < points.cols(); colIndex++) {
		fwrite(points.col(colIndex).data(), sizeof(float), points.rows(), fp);
		fwrite(&reflectivities(colIndex), sizeof(uint), 1, fp);
	}

	nb_points += points.cols();
	updateNumberOfPoints(fp);
	fclose(fp);
}

void PlyWriter::writePlyHeader()
{
	// Open file in binary mode.
	FILE *fp;
	fp = fopen(output_path.c_str(), "wb");
	if (!fp) {
		printf("[PlyWriter] Cannot create output ply file to %s\n", output_path.c_str());
		return;
	}

	fprintf(fp, "ply\n");
	fprintf(fp, "format binary_little_endian 1.0\n");

	// Save the point location to update.
	nb_point_location = ftell(fp);
	fprintf(fp, "element vertex %012d\n", 0);

	fprintf(fp, "property float x\n");
	fprintf(fp, "property float y\n");
	fprintf(fp, "property float z\n");
	fprintf(fp, "property uint reflectivity\n");
	fprintf(fp, "end_header\n");
	fclose(fp);
}

void PlyWriter::updateNumberOfPoints(FILE *fp) const
{
	fseek(fp, nb_point_location, SEEK_SET);
	fprintf(fp, "element vertex %011lu\n", nb_points);
	fseek(fp, 0, SEEK_END);
}