// Standard headers
#include <memory>
#include <regex>

// Osef headers
#include "osefTypes.h"

// Local headers
#include "osefParser.h"

// Check for Little/Big endian architecture.
// Returns true if BigEndian.
bool isBigEndianArchitecture()
{
	const uint32_t i = 0x01020304;
	return reinterpret_cast<const uint8_t *>(&i)[0] == 1;
}

OsefParser::OsefParser(const std::string &inputPath, bool autoReconnect)
	: input_path(""), auto_reconnect(autoReconnect), ip_v4(""), port(TcpStreamReader::default_port)
{
	parseInputArguments(inputPath);
}

int OsefParser::parse()
{
	if (!ip_v4.empty()) {
		return parseLiveStream();
	} else {
		return parseRecordStream();
	}
}

void OsefParser::parseInputArguments(const std::string &inputPath)
{
	const std::regex tcp_uri(TcpStreamReader::tcp_regex);
	std::smatch matches;
	if (std::regex_match(inputPath, matches, tcp_uri)) {
		// Live stream (tcp URI)
		ip_v4 = matches[1].str();
		port = matches[3].matched ? std::stoi(matches[3].str()) : TcpStreamReader::default_port;
		printf("[OsefParser] Parsing live stream from %s, on port %d.\n", ip_v4.c_str(), port);
	} else {
		input_path = inputPath;
		printf("[OsefParser] Parsing recorded stream from %s\n", input_path.c_str());
	}
}

int OsefParser::parseLiveStream()
{
	connectToLiveStream();

	int ret = 0;
	size_t bufferSize = 100'000'000;
	std::unique_ptr<uint8_t[]> buffer = std::make_unique<uint8_t[]>(bufferSize);

	while (ret != -1) {
		ret = tcp_reader.getNextFrame(buffer.get(), bufferSize);

		if ((ret == 0) && auto_reconnect) {
			connectToLiveStream();
		} else if (ret == 1) {
			ret = parseFrame((Tlv::tlv_s *)buffer.get());
		} else {
			return ret;
		}
	}

	return ret;
}

int OsefParser::parseRecordStream()
{
	FILE *fd = fopen(input_path.c_str(), "r");
	int ret = 0;

	if (fd == nullptr) {
		printf("[OsefParser] Error: cannot open recorded stream path : %s\n", input_path.c_str());
		return -1;
	}

	// Let reader allocate the buffer, and free it at the end.
	size_t bufferSize = 0;
	uint8_t *buffer = nullptr;
	Tlv::tlv_s *readTlv = nullptr;
	while ((readTlv = Tlv::readFromFile(fd, buffer, bufferSize)) && (ret != -1))
		ret = parseFrame(readTlv);

	free(buffer);
	fclose(fd);

	// Reached end of file
	return ret;
}

bool OsefParser::checkValidParsing(const Tlv::tlv_s *frame)
{
	// Check expected root TLV type.
	if (frame->getType() != OSEF_TYPE_TIMESTAMPED_DATA) {
		printf("[OsefParser] Error: wrong frame TLV type\n");
		return false;
	}

	// Value parsing on big endian architecture is not supported by this code.
	if (isBigEndianArchitecture()) {
		printf("[OsefParser] Value parsing on big endian architecture is not supported\n");
		return false;
	}

	return true;
}

void OsefParser::connectToLiveStream()
{
	if (!ip_v4.empty()) {
		int ret = 0;
		tcp_reader.disconnectfromShift();
		ret = tcp_reader.connectToShift(ip_v4.c_str(), port);

		if (ret == 0) {
			printf("[OsefParser] Shift not available yet (processing not started ?) actively waiting "
			       "for it...\n");
			while ((ret = tcp_reader.connectToShift(ip_v4.c_str(), port)) == 0)
				;
		}
	}
}