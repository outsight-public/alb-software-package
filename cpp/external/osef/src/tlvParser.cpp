#include <cassert>
#include <cstdlib>
#include <cstring>
#include <stdint.h>

#include "osefTypes.h"

#include "tlvParser.h"

namespace Tlv
{
int checkBufferSize(uint8_t *&buffer, size_t &buffer_size, size_t size_requested)
{
	if (buffer_size >= size_requested)
		return 0;

	buffer = (uint8_t *)realloc(buffer, size_requested);
	if (!buffer) {
		buffer_size = 0;
		return -1;
	}

	buffer_size = size_requested;
	return 0;
}

tlv_s *readFromFile(FILE *in_fd, uint8_t *&out_buffer, size_t &out_buffer_size)
{
	size_t read_sz;
	size_t length;

	// Read the header (T+L)
	if (checkBufferSize(out_buffer, out_buffer_size, sizeof(header_s)) < 0) {
		printf("[TLVParser] Failed to reallocate buffer\n");
		return nullptr;
	}
	read_sz = fread(out_buffer, sizeof(header_s), 1, in_fd);
	if (read_sz != 1)
		return nullptr;

	tlv_s *root = (tlv_s *)out_buffer;
	length = Tlv::getSize(*root);
	assert(length >= sizeof(header_s));

	// Read the value (V)
	if (checkBufferSize(out_buffer, out_buffer_size, length) < 0) {
		printf("[TLVParser] Failed to reallocate buffer\n");
		return nullptr;
	}
	read_sz = fread(out_buffer + sizeof(header_s), length - sizeof(header_s), 1, in_fd);
	if (read_sz != 1)
		return nullptr;

	return (tlv_s *)out_buffer;
}

Parser::Parser(const uint8_t *buffer_in, size_t buffer_size_in)
{
	buffer = buffer_in;
	buffer_size = buffer_size_in;
	next_index = 0;
};

tlv_s *Parser::getFirstTlv(void)
{
	size_t root_index = 0;

	return iter(root_index);
};

tlv_s *Parser::nextTlv(void)
{
	return iter(next_index);
};

tlv_s *Parser::findTlv(type_t type) const
{
	tlv_s *current_tlv;
	size_t index = 0;

	while ((current_tlv = iter(index)))
		if (current_tlv->getType() == type)
			break;

	return current_tlv;
};

tlv_s *Parser::iter(size_t &index) const
{
	tlv_s *next_tlv = (tlv_s *)(buffer + index);
	size_t next_size = 0;

	// Check that reading header element is not out of bound
	// Can be equal to buffer_size for latest element with a size of 0
	if (index + sizeof(header_s) > buffer_size)
		return nullptr;

	next_size = getSize(*next_tlv);

	// Check that the final size is not out of bound
	// Can be equal to buffer_size for the latest element
	if (index + next_size > buffer_size) {
		printf("[TLVParser] Next element is out of bound\n");
		return nullptr;
	}

	index += next_size;

	return next_tlv;
};

int Parser::parseTimestamp(const uint8_t *buffer, size_t buffer_size, timeval &output)
{
	Parser root_parser(buffer, buffer_size);

	tlv_s *read = root_parser.getFirstTlv();
	if (!read || read->getType() != OSEF_TYPE_TIMESTAMP_MICROSECOND || read->getLength() != 2 * sizeof(uint32_t)) {
		printf("[TLVParser] Bad TLV element passed to parseTimestamp\n");
		return -1;
	}

	// Do not memcpy!, Linux timeval can be defined as 2x32bits or 2x64bits fields.
	// That's why we explicitly init the struct with 2x32bits.
	uint32_t *casted = (uint32_t *)read->getValue();
	output.tv_sec = casted[0];
	output.tv_usec = casted[1];

	return 0;
}

} // namespace Tlv
