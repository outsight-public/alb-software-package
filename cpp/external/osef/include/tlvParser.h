#pragma once

#include <sys/time.h>

#include "tlvCommon.h"

namespace Tlv
{
// Read a TLV from a file descriptor
// Will try to use the passed buffer (and reallocate it if needed) to form the TLV.
// Pass out_buffer=nullptr and out_buffer_sz=0 to let the fuction allocate memory.
// in all cases, the caller is in charge of freeing out_buffer.
tlv_s *readFromFile(FILE *in_fd, uint8_t *&out_buffer, size_t &out_buffer_sz);

class Parser {
    public:
	Parser(const uint8_t *buffer, size_t buffer_size);

	// Return the first TLV in the buffer.
	// If corruption is detected (incorrect length), it will return nullptr.
	tlv_s *getFirstTlv(void);

	// Repeatedly call this function to get the next Tlv in the buffer.
	// It returns nullptr when no more is found (or if one of the TLV is corrupted)
	tlv_s *nextTlv(void);

	// Iterate through Tlvs and return the first one with specified type,
	// or nullptr if none was found
	tlv_s *findTlv(type_t type) const;

	static int parseTimestamp(const uint8_t *buffer, size_t buffer_size, timeval &output);

    private:
	const uint8_t *buffer = nullptr;
	size_t buffer_size = 0;
	size_t next_index = 0; // index used by nextSubTlv

	tlv_s *iter(size_t &index) const;
};

} // namespace Tlv
