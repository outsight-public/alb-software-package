"""Code sample to show how to filter reflectivity points of tracked object that are part of augmented cloud"""
# Standard imports
from argparse import ArgumentParser

import osef

# Third party imports
import plotly.graph_objects as go
from osef import osef_frame


def filter_object_reflectivity(input_osef: str) -> None:
    """
    Filter points that are part of objects and plot them with their reflectivity

    :param input_osef: path or url to the osef file or stream.
    """
    for frame_dict in osef.parse(input_osef, first=1):
        # extract data
        augmented_cloud = osef_frame.AugmentedCloud(frame_dict)
        coordinate_cloud = augmented_cloud.cartesian_coordinates
        object_reflectivity_cloud = augmented_cloud.reflectivities
        object_id_cloud = augmented_cloud.object_ids

        # First extract tracked objects
        object_points = coordinate_cloud[:, object_id_cloud != 0]
        object_point_reflectivity = object_reflectivity_cloud[object_id_cloud != 0]
        object_point_ids = object_id_cloud[object_id_cloud != 0]

        # Then extract backgroud
        background_points = coordinate_cloud[:, object_id_cloud == 0]

        # Plot tracked objects
        marker_data = go.Scatter3d(
            name="tracked object",
            x=object_points[0],
            y=object_points[1],
            z=object_points[2],
            mode="markers",
            marker=dict(
                size=6.0,
                color=object_point_reflectivity,
                colorscale="Portland",
                opacity=1.0,
                showscale=True,
            ),
            text=object_point_ids,
            hovertemplate="<b>Object ID</b>: %{text}"
            + "<br><b>X</b>: %{x}"
            + "<br><b>Y</b>: %{y}"
            + "<br><b>Z</b>: %{z}",
        )

        layout = go.Layout(scene=dict(aspectmode="data"))  # data: equal aspect ratios
        fig = go.Figure(data=marker_data, layout=layout)

        # Plot background
        fig.add_scatter3d(
            name="background",
            x=background_points[0],
            y=background_points[1],
            z=background_points[2],
            mode="markers",
            marker=dict(
                size=6.0,
                color="slategrey",
                opacity=0.2,
            ),
        )

        fig.update_layout(
            hoverlabel_font_size=23,
            font_family="Averta",
            hoverlabel_font_family="Averta",
        )
        fig.show()


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        description="Code sample to show how to filter reflectivity points that are part of augmented cloud."
    )
    arg_parser.add_argument(
        "input",
        type=str,
        help="File path to recorded OSEF file. "
        "TCP stream are also accepted on the form of tcp://host[:port].",
    )
    args = arg_parser.parse_args()
    filter_object_reflectivity(args.input)
