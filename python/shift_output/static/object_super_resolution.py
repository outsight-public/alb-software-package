"""Code sample to show how to build a super resolution
point cloud of an object (using multiple scan frames).

Requires:
    - Augmented OS 5.3 or >
    - Object super resolution feature activated (see Shift API):
        ProcessConfig:
            { "tracking":
                { "objects_super_resolution":
                    {"enable": true}
                }
            }
"""
# Standard imports
import logging
import pathlib
from argparse import ArgumentParser
from itertools import chain
from typing import List, Optional

# Third party imports
import numpy as np
from numpy import typing as npt
from numpy.lib.recfunctions import unstructured_to_structured
import plotly.graph_objects as go
import plyfile


import osef
from osef import osef_frame

logging.root.setLevel(logging.INFO)


def build_object_point_cloud(input_osef: str, output_directory: str) -> None:
    """Accumulate object points through time, save them in a ply file,
    then plot the resulting super resolution cloud.

    :param input_osef: path or url to the osef file or stream.
    :param output_directory: path to the directory in which the ply files will be saved.
    """
    slam_object_id: Optional[npt.NDArray[np.float32]] = None
    super_cloud: List[List[npt.NDArray[np.float32]]] = [[], [], []]
    list_reflectivities: List[npt.NDArray[np.int_]] = []
    for index, frame_dict in enumerate(osef.parse(input_osef)):
        if index % 100 == 0:
            logging.info(f"frame processed: {index}")

        # extract data
        augmented_cloud = osef_frame.AugmentedCloud(frame_dict)
        tracked_objects = osef_frame.TrackedObjects(frame_dict)
        object_ids = tracked_objects.object_ids

        # save and display previous object if it is not tracked anymore
        if slam_object_id is not None and (slam_object_id not in object_ids):
            super_clouds = np.array(
                [list(chain.from_iterable(super_cloud[i])) for i in range(3)]
            )
            reflectivities = np.array(list_reflectivities)
            output_path = pathlib.Path(output_directory).joinpath(
                f"object_{slam_object_id}.ply"
            )
            save_point_cloud(super_clouds, reflectivities, output_path)
            display_point_cloud(super_clouds, reflectivities)

            slam_object_id = None
            super_cloud = [[], [], []]
            list_reflectivities = []

        # get object slam pose if the slam algorithm is running on it
        for i, object_id in enumerate(object_ids):
            if tracked_objects.object_properties[i].has_valid_slam_pose:
                slam_pose = tracked_objects.slam_poses[i]
                translation = slam_pose.translation
                rotation = slam_pose.rotation
                slam_object_id = object_id
                break
        else:
            continue

        # compute object point cloud in object frame
        coordinate_cloud = augmented_cloud.cartesian_coordinates

        # filter object points
        object_id_cloud = augmented_cloud.object_ids

        object_pose_3x4 = np.hstack(
            (
                rotation,
                np.transpose([translation]),
            )
        )
        object_pose = np.vstack((object_pose_3x4, [0, 0, 0, 1]))

        object_abs_cloud = coordinate_cloud[:, object_id_cloud == slam_object_id]
        object_abs_cloud = np.vstack(
            (object_abs_cloud, np.ones(object_abs_cloud.shape[1]))
        )  # add line of w = 1 to apply transform matrix (inverse of pose)
        object_cloud = np.dot(np.linalg.inv(object_pose), object_abs_cloud)
        for i in range(3):
            super_cloud[i].append(object_cloud[i])

        point_reflectivities = augmented_cloud.reflectivities[
            object_id_cloud == slam_object_id
        ]
        list_reflectivities += point_reflectivities.tolist()


def save_point_cloud(
    super_cloud: npt.NDArray[np.int32],
    reflectivities: npt.NDArray[np.int32],
    output_path: pathlib.Path,
) -> None:
    """
    Save point cloud to ply file

    :param super_cloud: cloud points of the tracked objects
    :param reflectivities: reflectivities points of the tracked objects
    :param output_path: path to the directory in which the ply files will be saved
    """

    logging.info(f"Super resolution done, saving ply file: {output_path}.")
    vertices = unstructured_to_structured(
        np.vstack((super_cloud, reflectivities)).T,
        dtype=np.dtype([("x", "f4"), ("y", "f4"), ("z", "f4"), ("r", np.uint8)]),
    )
    plyfile.PlyData(
        [
            plyfile.PlyElement.describe(
                vertices,
                "vertex",
            )
        ]
    ).write(output_path)


def display_point_cloud(
    super_cloud: npt.NDArray[np.int32], reflectivities: npt.NDArray[np.int32]
) -> None:
    """
    Display object super resolution point cloud

    :param super_cloud: cloud points of the tracked objects
    :param reflectivities: reflectivities points of the tracked objects
    """
    logging.info("Displaying point cloud.")

    marker_data = go.Scatter3d(
        x=super_cloud[0],
        y=super_cloud[1],
        z=super_cloud[2],
        mode="markers",
        marker=dict(
            size=3.0,
            color=reflectivities,
            colorscale="Viridis",
            opacity=1.0,
        ),
    )
    layout = go.Layout(scene=dict(aspectmode="data"))  # data: equal aspect ratios
    fig = go.Figure(data=marker_data, layout=layout)
    fig.show()


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Code sample to show how to build a super resolution "
        "point cloud of an object (using multiple scan frames)."
    )
    parser.add_argument(
        "input",
        type=str,
        help="File path to recorded OSEF file. "
        "TCP stream are also accepted on the form of tcp://host[:port].",
    )
    parser.add_argument(
        "--output",
        type=str,
        default=".",
        help="Directory path in which generated ply will be saved.",
    )
    args = parser.parse_args()
    build_object_point_cloud(args.input, args.output)
