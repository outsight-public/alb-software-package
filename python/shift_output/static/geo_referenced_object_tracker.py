"""Extract data from the OSEF file, output a CSV file and display all tracked objects on a map"""
# Standard imports
import argparse
import csv
from typing import Any, Dict, List

# Third party imports
import pandas as pd
import plotly.express as px
import osef
from osef import osef_frame

CLASS_NAME = "class_name"
CLASS_ID = "class_id"
LATITUDE = "latitude"
LONGITUDE = "longitude"
OBJECT_ID = "object_id"
TIMESTAMP = "timestamp"


def geo_referenced_osef_to_csv(osef_stream: str, file_path: str) -> None:
    """
    Write in a csv file the geographic latitude and longitude of all tracked objects
    and sort them by class id.

    :param osef_stream: path to the osef file to TCP stream if path has form *tcp://hostname:port*
    :param file_path: path to the csv file
    """
    csv_rows: List[Dict[str, Any]] = []

    # extract object data from OSEF stream
    for frame in osef.parse(osef_stream, auto_reconnect=False):
        tracked_objects = osef_frame.TrackedObjects(frame)
        # Save every frame for all tracked objects.
        for object_index in range(tracked_objects.number_of_objects):
            row = {
                TIMESTAMP: tracked_objects.timestamp,
                CLASS_NAME: tracked_objects.class_names[object_index],
                CLASS_ID: tracked_objects.class_ids[object_index],
                OBJECT_ID: tracked_objects.object_ids[object_index],
                LATITUDE: tracked_objects.geographic_poses[object_index].latitude,
                LONGITUDE: tracked_objects.geographic_poses[object_index].longitude,
            }

            csv_rows.append(row)
            print(row)

    # Write in csv file
    with open(file_path, "w", encoding="UTF8", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=csv_rows[0].keys())
        writer.writeheader()
        writer.writerows(csv_rows)


def display_map_from_csv(file_path: str) -> None:
    """
    Display a map with the information defined in the csv file

    :param file_path: path to the csv file
    """
    # read csv file.
    csv_rows = pd.read_csv(file_path)
    fig = px.scatter_mapbox(
        data_frame=csv_rows,
        lon=csv_rows[LONGITUDE],
        lat=csv_rows[LATITUDE],
        animation_frame=csv_rows[TIMESTAMP],
        title="geo referenced tracked objects",
        hover_name=csv_rows[OBJECT_ID],
        hover_data=(csv_rows[CLASS_NAME], csv_rows[OBJECT_ID]),
        color=csv_rows[CLASS_ID],
        range_color=(0, 8),
        zoom=18,
    )

    # Display the map.
    fig.update_layout(mapbox_style="open-street-map", title=dict(y=1, x=0.5))
    fig.update_traces(marker_size=12, selector=dict(mode="markers"))

    # Select the animation speed.
    fig.layout.updatemenus[0].buttons[0].args[1]["frame"]["duration"] = 80
    fig.show()


# As a script, will unpack all the files passed as command line arguments.
if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(
        description="Decode osef file in from Shift in static lidar "
        "application and output CSV file"
    )
    arg_parser.add_argument(
        "input",
        type=str,
        help="File to be decoded (example: file.osef). "
        "Tcp stream are accepted on the form of tcp://host:port.",
    )
    arg_parser.add_argument(
        "--output",
        "-o",
        type=str,
        default="./georeferenced_tracked_object.csv",
        help="Path to the output csv file (default: ./georeferenced_tracked_object.csv).",
    )

    args = arg_parser.parse_args()

    geo_referenced_osef_to_csv(args.input, args.output)
    display_map_from_csv(args.output)
