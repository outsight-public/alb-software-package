"""Plot trajectories of tracked objects available in an OSEF stream.
"""
# Standard imports
import argparse
from typing import Any, Dict

# Third party imports
import numpy as np
import osef
from matplotlib import pyplot as plt
from osef import osef_frame


# This is important for 3d plotting
from mpl_toolkits.mplot3d import Axes3D  # pylint: disable=unused-import


def plot_trajectories(osef_stream: str) -> None:
    """
    Plot trajectories of tracked objects available in an OSEF stream.

    :param osef_stream: path to osef file or TCP stream if path has form *tcp://hostname:port*
    The sever may close the socket if client is too late.
    """
    # extract object data from OSEF stream
    objects: Dict[str, Dict[str, Any]] = {}
    for frame_dict in osef.parse(osef_stream, auto_reconnect=False):
        tracked_objects = osef_frame.TrackedObjects(frame_dict)

        for object_id, pose, object_class in zip(
            tracked_objects.object_ids,
            tracked_objects.poses,
            tracked_objects.class_names,
        ):
            tracked_object = objects.get(
                object_id,
                {
                    "trajectory": [],
                },
            )
            tracked_object["trajectory"].append(pose.translation)
            tracked_object["class"] = object_class
            objects[object_id] = tracked_object

    # plot trajectories
    figure = plt.figure()
    ax_2d = figure.add_subplot(121)
    ax_2d.set_xlabel("x (m)")
    ax_2d.set_ylabel("y (m)")
    ax_2d.set_title("Tracked object trajectories")

    ax_3d = figure.add_subplot(122, projection="3d")
    ax_3d.set_xlabel("x (m)")
    ax_3d.set_ylabel("y (m)")
    ax_3d.set_zlabel("z (m)")

    for object_id, tracked_object in objects.items():
        trajectory = tracked_object["trajectory"]
        trajectory_3n = np.transpose(trajectory)
        ax_2d.plot(trajectory_3n[0], trajectory_3n[1])
        ax_2d.text(
            trajectory[-1][0],
            trajectory[-1][1],
            f"{tracked_object['class']} {object_id}",
        )

        # 3d plot
        ax_3d.plot(trajectory_3n[0], trajectory_3n[1], trajectory_3n[2])

    # adjust axis aspect ration to have x_scale = y_scale
    if hasattr(ax_3d, "set_box_aspect"):  # not available in older versions
        ax_3d.set_box_aspect(
            [ub - lb for lb, ub in (getattr(ax_3d, f"get_{a}lim")() for a in "xy")]
            + [10]
        )
    plt.show()


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(
        description="Plot trajectories of tracked objects available in an OSEF stream."
    )
    arg_parser.add_argument(
        "input",
        type=str,
        help="File to be plotted (example: file.osef). "
        "Tcp stream are accepted on the form of tcp://host[:port]. ",
    )
    args = arg_parser.parse_args()
    plot_trajectories(args.input)
