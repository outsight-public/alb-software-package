"""As a script, will unpack all the files passed as command line arguments and print them.

Example:
-> TLV #0
 timestamped_data                                          (type: 20)
 │
 ├── timestamp_microseconds                                 (type: 12, value: 8 B)
 │
 │   {'remaining_us': 33196, 'unix_s': 1615450541}
 │
 │
 ├── scan_frame                                             (type: 25)
 │   │
 │   ├── pose                                               (type: 24, value: 48 B)
 │   │
 │   │   {'rotation': array([[-0.20957723,  0.97375441,  0.08876748],
 │   │          [-0.97582984, -0.21404146,  0.04407131],
 │   │          [ 0.06191456, -0.07738562,  0.99507684]]),
 │   │    'translation': array([ 4.0090971 , -7.73902893,  0.36036074])}
 ...
"""
import os
import pprint
from argparse import ArgumentParser
from collections import namedtuple
from typing import List

import numpy as np
from osef.parsing import parser, _parser_common, osef_stream
from osef.spec import constants, _types

BColorsNamedTuple = namedtuple(
    "BColorsNamedTuple",
    [
        "HEADER",
        "WARNING",
        "ENDC",
    ],
)

BColors = BColorsNamedTuple(HEADER="\033[95m", WARNING="\033[93m", ENDC="\033[0m")


def get_tree_representation(
    raw_tree: constants._TreeNode, _depth: int = 0, print_data: bool = True
) -> str:
    """
    Recursive function to generate a string representing
    the tlv tree structure and the values of the leaves.

    :param raw_tree: tlv tree to generate the string representation of.
    :param _depth: in the tree, used for indentation.
    :param print_data: parse and print data contained (only prints structure if equal to False).
    :return: string representation of the tree.
    """

    indent1 = " │  " * (_depth - 1) + " ├── " if _depth else " "
    indent2 = " │  " * (_depth - 1) + " │   " if _depth else " "

    osef_type = raw_tree.osef_type
    value = raw_tree.leaf_value
    type_info = _types.get_type_info_by_id(osef_type)
    output = indent2 + "\n" if _depth else ""
    output += (indent1 + type_info.name).ljust(60)
    if value is None:
        output += f"(type: {osef_type})\n"
    else:
        output += f"(type: {osef_type}, value: {len(value)} B)\n"

    if value and print_data:
        with np.printoptions(threshold=30):
            leaf_repr = pprint.pformat(
                _parser_common.unpack_value(value, type_info.node_info)
            )
        output += indent2 + "\n"
        output += indent2.join((indent2 + leaf_repr).splitlines(True)) + "\n"
        output += indent2 + "\n"

    if raw_tree.children:
        for child in raw_tree.children:
            output += get_tree_representation(child, _depth + 1, print_data)

    return output


def print_osef(
    inputs: List[str], first: int = None, last: int = None, print_data: bool = True
) -> None:
    """Parse and pretty print the content of an osef files (or streams).

    :param inputs: list of paths/url to parse and print.
    :param first: only display the N first items contained in each file
    :param last: only display the M last items contained in each file.
    Can be used with first to get the range (N-M)..N
    :param print_data: parse and print data contained (only prints structure if equal to False).
    :return: None
    """
    for path in inputs:
        print(BColors.HEADER + "-> Parsing %s" % path + BColors.ENDC)
        with osef_stream.create_osef_stream(path) as stream:
            if not stream:
                continue
            iterator = parser.get_tlv_iterator(stream, first, last)
            for idx, tlv in iterator:
                print(f"-> TLV #{idx}\n")
                raw_tree = parser.build_tree(tlv)
                if raw_tree:
                    print(get_tree_representation(raw_tree, print_data=print_data))


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        description="Decode files or streams with "
        "Outsight Serialization format (.osef extension) and pretty print them."
    )
    arg_parser.add_argument(
        "inputs",
        metavar="file.osef",
        type=str,
        nargs="+",
        help="Files to be decoded. Tcp stream are accepted on the form of tcp://host[:port]. "
        "String '-' means standard input (use with env var PYTHONUNBUFFERED=Y).",
    )
    arg_parser.add_argument(
        "--first",
        metavar="N",
        type=int,
        help="Only display the N first items contained in each file",
    )
    arg_parser.add_argument(
        "--last",
        metavar="M",
        type=int,
        help="Only display the M last items contained in each file. "
        "Can be used with first to get the range (N-M)..N",
    )
    arg_parser.add_argument(
        "--print-data",
        action="store_const",
        const=True,
        default=False,
        help="Print also data contained (print only structure by default).",
    )

    args = arg_parser.parse_args()

    if "-" in args.inputs and not os.environ.get("PYTHONUNBUFFERED", ""):
        print(
            "WARNING: reading from stdin may wont work "
            "if PYTHONUNBUFFERED environment var is not set to a non empty string"
        )

    print_osef(args.inputs, args.first, args.last, args.print_data)
