"""Stream an OSEF file on a TCP socket.
Packets will be sent at the right timing,
to be in the same conditions as when we receive
the stream from Shift.
"""
import pathlib
import socket
import struct
import time
from argparse import ArgumentParser

from osef.parsing import osef_stream
from osef.parsing._parser import parse_to_dict
from osef.parsing._parser_common import get_tlv_iterator, build_tree, _STRUCT_FORMAT
from osef.spec import osef_types


def stream_osef_file(
    file_path: str, ip_address: str, port: int = 11120, repeat: bool = False
) -> None:
    """
    Stream an osef file on a TCP socket with the right timings.

    :param file_path: local path to the osef file to stream.
    :param ip_address: hostname or IP address
    :param port: to be used (typically 11120)
    :param repeat: stream the osef in a loop.
    """

    filepath = pathlib.Path(file_path)
    if not filepath.is_file():
        raise FileNotFoundError(f"File {file_path} could not be found.")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        # to avoid: [Errno 98] Address already in use.
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # connect socket to client
        server_socket.bind((ip_address, port))
        server_socket.listen(1)
        client_socket, _ = server_socket.accept()
        while True:
            with osef_stream.create_osef_stream(file_path) as stream:
                _stream_on_socket(stream, client_socket)
                if not repeat:
                    break


def _stream_on_socket(
    stream: osef_stream.OsefStream, connected_socket: socket.SocketType
) -> None:
    """Send OSEF frames to a client over an opened TCP socket at the right frequency"""
    iterator = get_tlv_iterator(stream)
    previous_timestamp = None
    parsing_start_time = time.time()
    for _, tlv in iterator:
        raw_tree = build_tree(tlv)
        if not raw_tree:
            continue

        frame_dict = parse_to_dict(raw_tree)
        timestamp = frame_dict[osef_types.OsefKeys.TIMESTAMPED_DATA.value][
            osef_types.OsefKeys.TIMESTAMP_MICROSECOND.value
        ]
        packet = struct.pack(
            _STRUCT_FORMAT % tlv.length,
            tlv.osef_type,
            tlv.length,
            tlv.value,
        )
        if previous_timestamp is None:
            previous_timestamp = timestamp
        parsing_duration = time.time() - parsing_start_time
        time.sleep(max(timestamp - previous_timestamp - parsing_duration, 0))
        try:
            connected_socket.send(packet)
        except ConnectionResetError:
            break
        parsing_start_time = time.time()
        previous_timestamp = timestamp


if __name__ == "__main__":
    parser = ArgumentParser(description="Stream an OSEF file on a TCP socket.")
    parser.add_argument(
        "osef_filepath",
        type=str,
        help="Filepath to the osef file to stream (example: path/to/record.osef)",
    )
    parser.add_argument(
        "ip",
        type=str,
        help="Hostname or IP address to which the TCP stream will be sent to.",
    )
    parser.add_argument(
        "--port",
        type=int,
        default=11120,
        help="Port number to which the TCP stream will be sent to.",
    )

    parser.add_argument(
        "--repeat",
        action="store_const",
        const=True,
        default=False,
        help="Stream the osef file in a loop.",
    )
    args = parser.parse_args()
    stream_osef_file(args.osef_filepath, args.ip, args.port, args.repeat)
