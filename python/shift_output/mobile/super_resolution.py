"""The goal is to process an OSEF stream in super-resolution
mode in order to obtain, and display, a .ply map.

Each point of the map is localized in an absolute frame of reference,
i.e. an absolute coordinate system where the origin is the pose
of the lidar at the first processed frame.

Super-resolution means building a cloud using several successive frames.
We use the pose of the lidar provided by Shift
to build the cloud in the absolute frame.

"""
# Standard imports
import logging
from argparse import ArgumentParser
from itertools import chain
from typing import List

# Third party imports
import numpy as np
from numpy import typing as npt
from numpy.lib.recfunctions import unstructured_to_structured
import osef
from osef import osef_frame
import plotly.graph_objects as go
import plyfile

logging.root.setLevel(logging.INFO)


def make_super_cloud(
    input_stream: str,
    output_path: str,
    frame_quantity: int,
    display_results: bool = False,
) -> None:
    """
    Append clouds from multiple scan frames in osef stream.
    We use the lidar pose to transform point coordinates
    from the relative frame to the absolute frame.
    Once we have processed every frame, we save the point cloud
    in a ply file, then we display it.

    :param input_stream: file/stream to be processed.
    For instance: path/to/file.osef or tcp//192.168.2.2:11120.
    :param output_path: Path of the saved ply file.
    :param frame_quantity: Number of scans to be used to build super cloud.
    :param display_results: Display super cloud in a browser.
    """
    super_cloud: List[List[npt.NDArray[np.float32]]] = [[], [], []]
    reflectivities: List[npt.NDArray[np.int_]] = []
    logging.info(
        f"parsing stream from {input_stream}, "
        f"and building super cloud with {frame_quantity} first frames.",
    )
    for index, frame_dict in enumerate(osef.parse(input_stream, auto_reconnect=False)):
        if not index % 20:
            logging.info(f"Progress: {index} / {frame_quantity}")

        scan_frame = osef_frame.ScanFrame(frame_dict)
        aug_cloud = osef_frame.AugmentedCloud(frame_dict)

        # transform: relative ref -> absolute ref
        coord_abs_ref = scan_frame.pose.rotation.dot(
            aug_cloud.cartesian_coordinates
        ) + np.transpose([scan_frame.pose.translation])

        for i in range(3):
            super_cloud[i].append(coord_abs_ref[i].tolist())
        reflectivities.append(aug_cloud.reflectivities.tolist())

        if index >= frame_quantity:
            break

    super_cloud_array = np.array(
        [list(chain.from_iterable(super_cloud[i])) for i in range(3)]
    )
    reflectivities_array = np.array(list(chain.from_iterable(reflectivities))).flatten()

    logging.info(f"Super resolution done, saving ply file: {output_path}.")
    vertices = unstructured_to_structured(
        np.vstack((super_cloud_array, reflectivities_array)).T,
        dtype=np.dtype([("x", "f4"), ("y", "f4"), ("z", "f4"), ("r", np.uint8)]),
    )
    plyfile.PlyData(
        [
            plyfile.PlyElement.describe(
                vertices,
                "vertex",
            )
        ]
    ).write(output_path)

    if display_results:
        display_result(super_cloud_array, reflectivities_array)


def display_result(
    super_cloud: npt.NDArray[np.float32], reflectivities: npt.NDArray[np.float32]
) -> None:
    """
    Display the point cloud

    :param super_cloud: list of the point cloud to plot
    :param reflectivities: reflectivity of the point cloud
    """
    logging.info("Displaying point cloud.")
    downsample_factor = 5  # for viz performance reasons

    marker_data = go.Scatter3d(
        x=super_cloud[0][::downsample_factor],
        y=super_cloud[1][::downsample_factor],
        z=super_cloud[2][::downsample_factor],
        mode="markers",
        marker=dict(
            size=1.0,
            color=reflectivities[::downsample_factor],
            colorscale="Viridis",
            opacity=0.8,
        ),
    )
    layout = go.Layout(scene=dict(aspectmode="data"))  # data: equal aspect ratios
    fig = go.Figure(data=marker_data, layout=layout)
    fig.show()


if __name__ == "__main__":
    parser = ArgumentParser(description="Build a cloud using multiple scan frames.\n")
    parser.add_argument(
        "input_stream",
        type=str,
        help="Filepath or tcp_url to be opened and processed.\n"
        "For instance: path/to/file.osef or tcp://192.168.2.2:11120 OR tcp://host:port.",
    )
    parser.add_argument(
        "--output",
        type=str,
        default="super_resolution.ply",
        help="File path to be used to save the resulting ply file.",
    )
    parser.add_argument(
        "-v",
        "--viz",
        action="store_true",
        default=False,
        help="Display result in your default browser.",
    )
    parser.add_argument(
        "--scans",
        type=int,
        default=30,
        help="Number of scans to be used to build super cloud.",
    )
    args = parser.parse_args()
    make_super_cloud(args.input_stream, args.output, args.scans, args.viz)
