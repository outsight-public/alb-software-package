"""Lists lidar poses from an OSEF file or stream to a csv file."""
# Standard imports
import argparse
from typing import Dict, List
from datetime import datetime
import csv

# Third party imports
from osef import osef_frame
import osef


def list_lidar_poses(osef_stream: str, file_path: str) -> None:
    """
    Write in a csv file a list of lidar poses or just print the list if no output defined.

    :param osef_stream: dictionary containing the parsed values of a tlv scan frame.
    :param file_path: path to the output csv
    """
    # defined csv if output defined
    if file_path:
        csv_rows: List[Dict[str, float]] = []

    # extract object data from OSEF stream
    for idx, frame in enumerate(osef.parse(osef_stream, auto_reconnect=False)):

        # Extract relevant fields from the OSEF
        scan_frame = osef_frame.ScanFrame(frame)
        lidar_pose = scan_frame.pose

        # Build a new row with all the information
        row = {
            "frame_number": idx,
            "timestamp": datetime.utcfromtimestamp(scan_frame.timestamp),
            "position_x": lidar_pose.translation[0],
            "position_y": lidar_pose.translation[1],
            "position_z": lidar_pose.translation[2],
            "rotation_xx": lidar_pose.rotation[0][0],
            "rotation_xy": lidar_pose.rotation[0][1],
            "rotation_xz": lidar_pose.rotation[0][2],
            "rotation_yx": lidar_pose.rotation[1][0],
            "rotation_yy": lidar_pose.rotation[1][1],
            "rotation_yz": lidar_pose.rotation[1][2],
            "rotation_zx": lidar_pose.rotation[2][0],
            "rotation_zy": lidar_pose.rotation[2][1],
            "rotation_zz": lidar_pose.rotation[2][2],
        }
        # If no output needed : just print data
        if file_path:
            csv_rows.append(row)
        else:
            print(row)

    if file_path and len(csv_rows) > 0:
        # Write in csv file if output defined
        with open(file_path, "w", encoding="UTF8", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=csv_rows[0].keys())
            writer.writeheader()
            writer.writerows(csv_rows)


# As a script, will unpack all the files passed as command line arguments
if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(
        description="Decode osef file in from Shift in moving lidar "
        "application and output csv with poses information."
    )
    arg_parser.add_argument(
        "input",
        type=str,
        help="File to be decoded (example: file.osef). "
        "Tcp stream are accepted on the form of tcp://host:port.",
    )
    arg_parser.add_argument(
        "output",
        metavar="output.csv",
        type=str,
        nargs="?",
        help="Path to csv file to save the output. Printed to stdout if omitted.",
    )
    args = arg_parser.parse_args()

    list_lidar_poses(args.input, args.output)
