"""To easily install code samples."""
from setuptools import setup, find_packages

with open("requirements.txt") as f:
    required = f.read().splitlines()

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="outsight_samples",
    version="0.0.2",
    packages=find_packages(exclude=["tests"]),
    author="Outsight Developers",
    author_email="support@outsight.tech",
    description="Osef examples.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=required,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
